function find(elements,cb){
    if(!elements || elements.constructor !== Array){
        return undefined;
    }
    if(!cb){
        return undefined;
    }
    let answer = undefined;
    for(let index=0;index<elements.length;index++){
        if(cb(elements[index]),index,elements){
            answer = elements[index];
            break;
        }
    }
    return answer;
}

module.exports = find;