function reduce(elements,cb,startingValue){
    if(!elements || elements.constructor !== Array){
        return [];
    }
    if(!cb){
        return elements;
    }
    let index = 0;
    if(!startingValue && startingValue !== 0){
        startingValue = elements[0];
        index = 1;
    }
    for(index;index<elements.length;index++){
        startingValue = cb(startingValue,elements[index],index,elements);
    }
    return startingValue;
}

module.exports = reduce;