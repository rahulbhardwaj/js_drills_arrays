function filter(elements,cb){
    if(!elements || elements.constructor !== Array){
        return [];
    }
    if(!cb){
        return elements;
    }
    let answer = [];
    for(let index=0;index<elements.length;index++){
        if(cb(elements[index],index,elements)){
            answer.push(elements[index]);
        }
    }
    return answer;
}

module.exports = filter;