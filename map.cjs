function map(elements,cb){
    if(!elements || elements.constructor !== Array){
        return [];
    }
    if(!cb){
        return elements;
    }
    let answer = [];
    for(let index=0;index<elements.length;index++){
        answer.push(cb(elements[index],index,elements));
    }
    return answer;
}

module.exports = map;