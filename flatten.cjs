function flatten(elements,level){
    if(!elements || elements.constructor !== Array){
        return [];
    }
    if(!level && level!==0){
        level = 1;
    }
    if(level===0){
        return elements;
    }
    for(let index=0;index<elements.length;index++){
        if(Array.isArray(elements[index]) || (!elements[index] && elements[index]!==0)){
            isArray = true;
        }
    }
    if(isArray === false){
        return elements;
    }

    let newArray = [];
    for(let index=0;index<elements.length;index++){
        if(!elements[index] && elements[index]!==0){
            continue;
        }
        if(elements[index].constructor === Array && level>0){
            let flatnewArray = flatten(elements[index],level-1)
            newArray =  newArray.concat(flatnewArray);
        }
        else{
            newArray.push(elements[index]);
        }
    }
    return newArray;
}

module.exports = flatten;