let items = require('../arrays.cjs');
let map = require('../map.cjs');

console.log(map(items,(a) => a*a));
console.log(map(items,(a) => a+'a'));
console.log(map());
console.log(map(items));
console.log(map({obj:'object'},(a) => a*a));
console.log(map('this is string',(a) => 2*a));
console.log(map([2.2,5,7.0],(a) => parseInt(a)));
console.log(map([2.2,5,7.0],parseInt));
console.log(map([2.2,5,7.0,,5,undefined,'string'],(a) => 4*a));