let items = require('../arrays.cjs');
let reduce = require('../reduce.cjs');

console.log(reduce(items,(a,b) => (a+b),0));
console.log(reduce(items,(a,b) => (a+b)));
console.log(reduce({obj:Object},(a,b) => (a+b),0));
console.log(reduce(items));