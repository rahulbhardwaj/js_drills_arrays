let items = require('../arrays.cjs');
let each = require('../each.cjs');

console.log(each(items,(a) => a*a));
console.log(each(items,(a) => a+'a'));
console.log(each());
console.log(each(items));
console.log(each({obj:'object'},(a) => a*a));