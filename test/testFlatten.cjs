const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
let flatten = require('../flatten.cjs');

console.log(flatten(nestedArray));
console.log(flatten(nestedArray,1));
console.log(flatten(nestedArray,2));
console.log(flatten(nestedArray,Infinity));
console.log(flatten([[9],0,9,,8,[[[8]]]])); //with empty element
console.log(flatten([5,4,8,7,9],Infinity));
console.log(flatten([5,4,8,7,9].flat(Infinity),Infinity));
console.log([5,4,8,7,9].flat(Infinity));
console.log(flatten([5,4,[8,7],"a"],0));
console.log(flatten([]));
console.log(flatten());
console.log(flatten({obj:'object'}));
console.log(JSON.stringify(flatten([5,4,8,7,9]))===JSON.stringify([5,4,8,7,9].flat()));
console.log(JSON.stringify(flatten([5,[4,[8]],[7,9]],Infinity))===JSON.stringify(flatten([5,4,8,7,9].flat(Infinity))));
console.log(flatten([8,9,0,9,,9]));
console.log(flatten([8,9,0,9,9]));